export default function(id, outTradeNo) {
	let selectedProvider = ''
	return new Promise((resolve, reject) => {
		uni.getProvider({
			service: 'payment',
			success(res) {
				resolve(res.provider)
			},
			fail() {
				reject(new Error('获取支付方式失败'))
			}
		})
	}).then((providerList) => {
		return new Promise((resolve, reject) => {
			uni.showActionSheet({
				itemList: providerList,
				success(res) {
					resolve(providerList[res.tapIndex])
				},
				fail() {
					reject(new Error('取消支付'))
				}
			})
		})

	}).then((provider) => {
		selectedProvider = provider
		return uniCloud.callFunction({
			name: 'zx-pay',
			data: {
				provider,
				outTradeNo: outTradeNo
			}
		})
	}).then((res) => {
		console.log(res);
		if (res.result.orderInfo) {
			return new Promise((resolve, reject) => {
				uni.requestPayment({
					// #ifdef APP-PLUS
					provider: selectedProvider,
					// #endif
					// #ifdef MP-WEIXIN
					...res.result.orderInfo,
					// #endif
					// #ifdef APP-PLUS || MP-ALIPAY
					orderInfo: res.result.orderInfo,
					// #endif
					complete() {
						resolve(res.result.outTradeNo)
					}
				})
			})
		} else {
			throw new Error(res.result.msg)
		}
	}).then((outTradeNo) => {
		return uniCloud.callFunction({
			name: 'zx-pay-query',
			data: {
				outTradeNo
			}
		})
	}).then((res) => {
		if (res.result.orderPaid) {
			// this.status = 2
			uni.showModal({
				content: '订单已支付',
				showCancel: false
			})
		} else {
			uni.showModal({
				content: '订单未支付',
				showCancel: false
			})
		}
	}).catch((err) => {
		uni.showModal({
			content: err.message || '支付失败',
			showCancel: false
		})
	})
}
