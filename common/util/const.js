// 请求服务端接口 动作名称
const ACTION = {
	ADD: 'add',
	DEL: 'del',
	EDIT: 'edit',
	QUERY: 'query',	
	LOGIN: 'login',
	LOGOUT: 'logout',
	ADD_ONE: 'addOne',
	LOGIN_BY_SMS: 'loginBySms',
	BIND_MOBILE: 'bindMobile',
	WX_LOGIN: 'wxLogin',
	REGISTER: 'register',
	QUERY_ONE: 'queryOne',
	QUERY_TWO: 'queryTwo',
	QUERY_THREE: 'queryThree',
	QUERY_BY_ID: 'queryById',
	CHECK_TOKEN: 'checkToken',
	EDIT_BY_NUM: 'editByNum',
	EDIT_BY_NAME: 'editByName',
	QUERY_BY_NAME: 'queryByName',
	QUERY_BY_TIME: 'queryByTime',
	SEND_SMS_CODE: 'sendSmsCode',
	VERIFY_CODE: 'verifyCode',
	GET_USER_INFO: 'getUserInfo',
	UPDATE_USER: 'updateUser'
}

const URL = {
	DASHBOARD: 'dashboard',
	ACCOUNT: 'account',
	POINT: 'point',
	ADDRESS: 'address',
	PAGE: 'page',
	COUPON_USER: 'coupon-user',
	COUPON: 'coupon',
	GOODS: 'goods',
	GOODS_GROUP: 'goods-group',
	ORDER: 'order',
	QRCODE: 'qrcode',
	SELFGET: 'selfget',
	SELFGET_USER: 'selfget-user',
	SETTING: 'setting',
	SHIPPING_TEMPLATE: 'shipping-template',
	SOURCE_GROUP: 'source-group',
	SOURCE: 'source',
	TABBAR: 'tabbar',
	USER: 'user'
}

//请求服务端接口 返回状态码
const CODE = {
	SUCCESS: 200,
	TOEKN_INVALID: 401,
	OTHER_TIP: 402
}

module.exports = {
	ACTION,
	CODE,
	URL
}

