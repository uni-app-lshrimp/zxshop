import request from '@/common/util/request'
import myConst from '@/common/util/const'
const url = myConst.URL.SETTING
export const getSettingData = (data) => {
	return request({
		url,
		check:false,
		action: myConst.ACTION.QUERY,
		data
	})
}