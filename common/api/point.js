import request from '@/common/util/request'
import myConst from '@/common/util/const'
const url = myConst.URL.POINT
export const getData = (data) => {
	return request({
		url,
		check:false,
		action: myConst.ACTION.QUERY,
		data
	})
}
export const getDataOne = (data) => {
	return request({
		url,
		check:false,
		action: myConst.ACTION.QUERY_ONE,
		data
	})
}
export const getPointDataOne = (data) => {
	return request({
		url,
		check:false,
		action: myConst.ACTION.QUERY_ONE,
		data
	})
}
export const getDataTwo = (data) => {
	return request({
		url,
		check:false,
		action: myConst.ACTION.QUERY_TWO,
		data
	})
}
export const getDataThree = (data) => {
	return request({
		url,
		check:false,
		action: myConst.ACTION.QUERY_THREE,
		data
	})
}
export const addPointData = (data) => {
	return request({
		url,
		check:false,
		action: myConst.ACTION.ADD,
		data
	})
}
export const addPointListData = (data) => {
	return request({
		url,
		check:false,
		action: myConst.ACTION.ADD_ONE,
		data
	})
}