import request from '@/common/util/request'
import myConst from '@/common/util/const'
const url = myConst.URL.TABBAR
export const getData = (data) => {
	return request({
		url,
		check:false,
		action: myConst.ACTION.QUERY,
		data
	})
}