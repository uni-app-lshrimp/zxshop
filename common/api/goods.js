import request from '@/common/util/request'
import myConst from '@/common/util/const'
const url = myConst.URL.GOODS
export const getData = (data) => {
	return request({
		url,
		check:false,
		action: myConst.ACTION.QUERY,
		data
	})
}
export const getDataOne = (data) => {
	return request({
		url,
		check:true,
		action: myConst.ACTION.QUERY_ONE,
		data
	})
}
export const getDataTwo = (data) => {
	return request({
		url,
		check:true,
		action: myConst.ACTION.QUERY_TWO,
		data
	})
}
export const getDataById = (data) => {
	return request({
		url,
		check:false,
		action: myConst.ACTION.QUERY_BY_ID,
		data
	})
}
export const getGoodsDataById = (data) => {
	return request({
		url,
		check:false,
		action: myConst.ACTION.QUERY_BY_ID,
		data
	})
}
export const editDataByNum = (data) => {
	return request({
		url,
		check:false,
		action: myConst.ACTION.EDIT_BY_NUM,
		data
	})
}