import request from '@/common/util/request'
import myConst from '@/common/util/const'
const url = myConst.URL.PAGE
export const getData = (data) => {
	return request({
		url,
		check:false,
		action: myConst.ACTION.QUERY,
		data
	})
}
export const getDataById = (data) => {
	return request({
		url,
		check:false,
		action: myConst.ACTION.QUERY_BY_ID,
		data
	})
}
export const getPageDataById = (data) => {
	return request({
		url,
		check:false,
		action: myConst.ACTION.QUERY_BY_ID,
		data
	})
}