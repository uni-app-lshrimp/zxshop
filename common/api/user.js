import request from '@/common/util/request'
import myConst from '@/common/util/const'
const url = myConst.URL.ACCOUNT
export const sendSmsCode = (data) => {
	return request({
		url,
		check:false,
		action: myConst.ACTION.SEND_SMS_CODE,
		data
	})
}
export const bindMobile = (data) => {
	return request({
		url,
		check:false,
		action: myConst.ACTION.BIND_MOBILE,
		data
	})
}
export const wxLogin = (data) => {
	return request({
		url,
		check:false,
		action: myConst.ACTION.WX_LOGIN,
		data
	})
}
export const loginBySms = (data) => {
	return request({
		url,
		check:false,
		action: myConst.ACTION.LOGIN_BY_SMS,
		data
	})
}
export const getUserInfo = (data) => {
	return request({
		url,
		check:false,
		action: myConst.ACTION.GET_USER_INFO,
		data
	})
}
export const updateUser = (data) => {
	return request({
		url,
		check:false,
		action: myConst.ACTION.UPDATE_USER,
		data
	})
}