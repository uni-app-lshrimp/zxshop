import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		hasLogin: false,
		userInfo: {},
		userId: '',
		userName: '',
		token: ''
	},
	mutations: {
		login(state,data) {
			state.userName = data.mobile
			state.userId = data.uid
			state.token = data.token
			if(JSON.stringify(state.userInfo) != "{}"){
				state.hasLogin = true	
			}
		},
		setUserInfo(state, userInfo) {
			state.userInfo = userInfo
			if(state.userId!=''){
				state.hasLogin = true
			}
		},
		logout(state) {
			state.hasLogin = false;
			state.userInfo = {};
			state.userId = '';
			state.userName = '';
			state.token = '';
			uni.clearStorageSync();
		},
	},
	actions: {

	}
})

export default store
