<h1 align="center">小新商城</h1> 
<p align="center">
<a href="https://gitee.com/zxshop/zxshop">
        <img src="https://img.shields.io/badge/Licence-GPL2.0-green.svg?style=flat" />
    </a>
    <a href="https://gitee.com/zxshop/zxshop">
        <img src="https://img.shields.io/badge/Edition-3.1-blue.svg" />
    </a>
</p>
<p align="center">    
    <b>如果对您有帮助，您可以点右上角 "Star" 支持一下 谢谢！</b>
</p>

# 小新商城

> 小新商城是一套基于uniapp+uniCloud开发的单商户商城系统，通过拖拽可视化组件，生成适用全行业多场景模板，帮商家快速网上开店。同时支持PC端后台管理和移动端后台管理，提高产品全方位用户体验。

## 支持组件
* 基础组件：文本、图片广告、商品、视频、标题、图文导航、商品搜索、公告、辅助空白、辅助线、自由面板
* 系统组件：分类模板、页面导航
* 营销组件：优惠券

## 体验商城微信小程序
<img src="https://vkceyugu.cdn.bspapp.com/VKCEYUGU-aliyun-ltwrba7ylozp833033/5d13bd80-499d-11eb-b997-9918a5dda011.jpg" height="255" width="255" >

## 体验商城管理后台（微信小程序端）
<img src="https://vkceyugu.cdn.bspapp.com/VKCEYUGU-aliyun-ltwrba7ylozp833033/f08aa1d0-49a9-11eb-8a36-ebb87efcf8c0.jpg" height="255" width="255" >

## 体验商城管理后台（H5端）
<img src="https://vkceyugu.cdn.bspapp.com/VKCEYUGU-aliyun-ltwrba7ylozp833033/0870af50-49ab-11eb-a16f-5b3e54966275.png" height="255" width="255" >

## 体验商城管理后台（PC端）
[在线预览](https://static-9239e7c1-2df8-48b7-a69c-9ddc5f5d8907.bspapp.com)

## 管理后台体验账号
* 用户名：admin
* 密码：314159

## 如需技术支持，部署指导，定制化开发，请加好友
![微信小程序](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-zxshop/71b98a30-e7a0-11ea-8a36-ebb87efcf8c0.jpg)

## 部署步骤

```bash
# 导入项目
将项目导入到HBuilderX开发工具中

# 修改根目录下manifest.json
AppID:从微信小程序获取

# 修改根目录下config.js
SPACE_ID:从unicloud服务空间获取SpaceID
CLIENT_SECRET:从unicloud服务空间获取clientSecret

# 修改配置文件 cloudfunctions/common/config
wxConfigMp中的
appId:从微信小程序获取
secret:从微信小程序获取
mchId:从支付商户平台获取
key:从支付商户平台获取
smsConfig中的
smsKey:从在Dcloud开发者中心获取
smsSecret:从在Dcloud开发者中心获取
templateId:从在Dcloud开发者中心获取
name:从在Dcloud开发者中心获取

# 修改配置文件 cloudfunctions/common/uni-id/config.json
mp-weixin中的
appId:从微信小程序获取
secret:从微信小程序获取
service中的
smsKey:从在Dcloud开发者中心获取
smsSecret:从在Dcloud开发者中心获取
templateId:从在Dcloud开发者中心获取
name:从在Dcloud开发者中心获取

# 关联云服务空间
没有云服务空间需要通过鼠标右键cloudfunctions，点击创建云服务空间，然后再关联刚刚创建好的云服务空间

# 初始化cloudfunctions/db_init.json
鼠标邮件db_init.json，点击初始化云数据库

# 上传cloudfunctions/common功能模块
逐个文件鼠标右键点击上传公共模块

# 上传cloudfuncions下的云函数
逐个点击以zx-开头的文件，鼠标右键点击上传部署

# 登录微信公众平台https://mp.weixin.qq.com
进入菜单 开发-开发设置 配置服务器域名
request合法域名 https://api.bspapp.com
socket合法域名 wss://api.bspapp.com
uploadFile合法域名 https://bsppub.oss-cn-shanghai.aliyuncs.com
downloadFile合法域名 https://vkceyugu.cdn.bspapp.com

# 运行项目
最后通过HBuilderX菜单栏运行-运行到浏览器，启动项目
```